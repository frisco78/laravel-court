<?php

use App\Http\Controllers\ProduitController;
 
Route::get('/produit/{id}', [ProduitController::class, 'show']);